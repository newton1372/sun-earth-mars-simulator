var calcolaPosizione=function(t,point,angularSpeed)
{
	var R = point.length();
	var positionAtTimeT = new BABYLON.Vector3();
	positionAtTimeT.x = R*Math.cos(angularSpeed*t);
	positionAtTimeT.z = R*Math.sin(angularSpeed*t);
	positionAtTimeT.y = point.y;
	return positionAtTimeT;
}


function avanzaPianetaDiDT(planet)
{
	//Questa funzione calcola numericamente col metodo di Eulero 
	//la velocità e la posizione di un pianeta che subisce un campo gravitazionale
	//creato da una stella posta nell'origine dopo un tempo dt, aggiornando poi
	//i membri dell'oggetto pianeta passato come riferimento.
	
	var M = 20.0;
	var G = 0.1;
	var dt = 0.01;
	var r = planet.posizione.length();
	var unitVersor = planet.posizione.normalizeToNew(); //versore posizione del pianeta
	var acceleration = unitVersor.scale(-M*G/Math.pow(r,2));
	var dv = acceleration.scale(dt);
        planet.velocità = planet.velocità.add(dv);
	var dr = planet.velocità.scale(dt);
	planet.posizione = planet.posizione.add(dr);	
}


function tickConEulero()
{
	avanzaPianetaDiDT(oggettoTerra);
	avanzaPianetaDiDT(oggettoMarte);
	terra.position.copyFrom(oggettoTerra.posizione);
	marte.position.copyFrom(oggettoMarte.posizione);
	
}


function tick()
{
	  t=t+0.01
   	  var posizioneTerra = calcolaPosizione(t,posizioneInizialeTerra,angularSpeedTerra);
    	  var posizioneMarte = calcolaPosizione(t,posizioneInizialeMarte,angularSpeedMarte);
    	  terra.position.copyFrom(posizioneTerra);
    	  marte.position.copyFrom(posizioneMarte);	
}



   
var canvas = document.getElementById('renderCanvas');
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
scene.clearColor = new BABYLON.Color3.Black();
// luce
var light = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0,0,0), scene);
light.intensity=5.0;

// telecamera
var camera = new BABYLON.ArcRotateCamera("Camera", 0, 0.8, 7, new BABYLON.Vector3.Zero(), scene);
camera.attachControl(canvas, true);

//materiali
var materiale1 = new BABYLON.StandardMaterial("Star", scene);
materiale1.diffuseColor = new BABYLON.Color3.Yellow();
materiale1.emissiveColor = new BABYLON.Color3.Yellow();

var materiale2 = new BABYLON.StandardMaterial("Earth", scene);
materiale2.diffuseColor = new BABYLON.Color3.Green();

var materiale3 = new BABYLON.StandardMaterial("Mars",scene);
materiale3.diffuseColor = new BABYLON.Color3.Red();



//Corpi celesti
var sole= new BABYLON.Mesh.CreateSphere("Sole",20,1.0, scene);
var terra = new BABYLON.Mesh.CreateSphere("Terra",20,0.3,scene);
var marte = new BABYLON.Mesh.CreateSphere("Marte",20,0.3,scene);
sole.material = materiale1;
terra.material = materiale2;
marte.material= materiale3;
var posizioneInizialeSole = new BABYLON.Vector3(0,0,0);
var posizioneInizialeTerra = new BABYLON.Vector3(2,0,0);
var posizioneInizialeMarte = new BABYLON.Vector3(5,0,0);//5
var velocitàInizialeTerra = new BABYLON.Vector3(0.3,0.5,0.9);
var velocitàInizialeMarte = new BABYLON.Vector3(0.1,0.3,0.5);//0.1,0.3,0.5
sole.position = posizioneInizialeSole;
terra.position = posizioneInizialeTerra;
marte.position = posizioneInizialeMarte;

var angularSpeedTerra = 2;
var angularSpeedMarte = 0.5;
var t =0;

var oggettoTerra={posizione: posizioneInizialeTerra,
	          velocità: velocitàInizialeTerra};

var oggettoMarte={posizione: posizioneInizialeMarte,
	          velocità: velocitàInizialeMarte};



engine.runRenderLoop(function(){   
    // disegno la scena
   	  tickConEulero();
	  scene.render();
});



	
